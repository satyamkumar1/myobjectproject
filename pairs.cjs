function getValuesOfObject(data) {

    if (typeof data !== 'object') {
        return [];
    }
    if(Array.isArray(data))
    {
        return [];
    }

    let listOfObjects = [];
    for (let index in data) {
        // let singleObj = {};
        // singleObj['key'] = index;
        // singleObj['value'] = data[index];
        listOfObjects.push([index,data[index]]);

    }


    return listOfObjects;

}
module.exports = getValuesOfObject;