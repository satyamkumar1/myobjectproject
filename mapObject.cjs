function mapObject(data,cb) {

        if(Array.isArray(data))
        {
            return {};
        }


    if (typeof data !== 'object') {
        return [];
    }

    const keysArray = [];
    for (let index in data) {
        
       data[index]=cb(data[index],index);
        

    }


    return data;

}
module.exports = mapObject;