function mapObject(data,defaultdata) {

    if(Array.isArray(data))
    {
        return {};
    }


if (typeof data !== 'object') {
    return {};
}

if(defaultdata==null || defaultdata==undefined)
{
    return data;
}
if (typeof defaultdata !== 'object') {
    return data;
}

for (let index in defaultdata) {
    if(data[index] == undefined)
    {
        data[index]=defaultdata[index];
    }

}
    

return data;

}
module.exports = mapObject;