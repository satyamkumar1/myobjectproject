function getValuesOfObject(data) {

    if (typeof data !== 'object') {
        return {};
    }
    if(Array.isArray(data))
    {
        return {};
    }

    const invertObject = {};
    for (let index in data) {
        invertObject[data[index]]=index;

    }


    return invertObject;

}
module.exports = getValuesOfObject;